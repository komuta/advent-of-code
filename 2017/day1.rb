#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/captcha'

captcha = Captcha.new(
  File.read('day1_input').strip
)

# Part 1
puts "Part 1: #{captcha.sum}"

# Part 2
puts "Part 2: #{captcha.new_sum}"
