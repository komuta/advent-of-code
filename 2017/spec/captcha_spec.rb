require_relative '../lib/captcha'

describe Captcha do
  subject do
    Captcha.new(numbers)
  end

  describe '#sum' do
    [
      ['1122', 3],
      ['1111', 4],
      ['1234', 0],
      ['91212129', 9]
    ].each do |numbers, sum|
      context "captcha with numbers #{numbers}" do
        let(:numbers) { numbers }

        it "computes the expected sum #{sum}" do
          expect(subject.sum).to eq(sum)
        end
      end
    end
  end

  describe '#new_sum' do
    [
      ['1212', 6],
      ['1221', 0],
      ['123425', 4],
      ['123123', 12],
      ['12131415', 4]
    ].each do |numbers, sum|
      context "captcha with numbers #{numbers}" do
        let(:numbers) { numbers }

        it "computes the expected sum #{sum}" do
          expect(subject.new_sum).to eq(sum)
        end
      end
    end
  end
end
