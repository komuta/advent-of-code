class Captcha
  def initialize(numbers)
    @numbers = numbers
  end

  def sum
    sequence = @numbers.each_char.map(&:to_i)
    sequence << sequence.first
    sequence.each_cons(2).inject(0) do |sum, pair|
      first, second = pair
      sum += first if first == second
      sum
    end
  end

  def new_sum
    sequence = @numbers.each_char.map(&:to_i)
    compare_steps = @numbers.size / 2
    sum = 0
    sequence.each_with_index do |number, i|
      compare_to_index = (i + compare_steps) % @numbers.size
      sum += number if number == sequence[compare_to_index]
    end
    sum
  end
end
