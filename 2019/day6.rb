#!env ruby
# frozen_string_literal: true

require_relative 'lib/astral_object.rb'

index = AstralObjectIndex.new
index.parse(File.read('day6_input'))
puts index.orbits_count
# part 1: 308790

you = index.object('YOU')
san = index.object('SAN')
puts index.orbital_transfers_between(you, san)
# part 2: 472
