#!env ruby
# frozen_string_literal: true

require_relative 'lib/intcode/computer.rb'

memory = Intcode::Memory.read(File.read('day5_input'))
computer = Intcode::Computer.new(memory, StringIO.new('1'), STDOUT)
computer.run

# 1st part: 5182797

memory = Intcode::Memory.read(File.read('day5_input'))
computer = Intcode::Computer.new(memory, StringIO.new('5'), STDOUT)
computer.run

# 2nd part: 12077198
