#!env ruby
# frozen_string_literal: true

require_relative 'lib/wire_path.rb'

w1 = WirePath.read('R8,U5,L5,D3')
w2 = WirePath.read('U7,R6,D4,L4')
w1 = WirePath.read('R75,D30,R83,U83,L12,D49,R71,U7,L72')
w2 = WirePath.read('U62,R66,U55,R34,D71,R55,D58,R83')

w1, w2 = File.read('day3_input').split("\n").map do |wire_path|
  WirePath.read(wire_path)
end

inter = w1.intersections(w2)

puts inter.map { |point| WirePath.origin.distance_to(point) }.min
# require 'pry'
# binding.pry

# w1.edges.each { |edge| puts edge }
# puts w1
# puts w2
# puts w1.horizontal_edges

puts inter.map { |point| w1.steps_to(point) + w2.steps_to(point) }.min
