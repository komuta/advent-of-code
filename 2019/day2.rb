#!env ruby
# frozen_string_literal: true

require_relative 'lib/intcode/program.rb'

def test(first, second)
  program = Intcode::Program.read(File.read('day2_input'))
  program.write(1, first)
  program.write(2, second)
  program.run!
end

result = test(12, 2)
puts result.join(',')
# correct anwser:  3101878

(0..99).each do |noun|
  (0..99).each do |verb|
    result = test(noun, verb)
    next unless result.first == 19_690_720

    answer = 100 * noun + verb
    puts "found! noun=#{noun}, verb=#{verb}, answer=#{answer}"
    break
  end
end

# correct answer: 8444
