# frozen_string_literal: true

require_relative '../lib/fuel_calculator.rb'

describe FuelCalculator do
  subject do
    FuelCalculator.new
  end

  describe '#mass_to_fuel' do
    context 'module mass' do
      [
        [12, 2],
        [14, 2],
        [1969, 654],
        [100_756, 33_583]
      ].each do |mass, expected_fuel|
        it "converts mass #{mass} to fuel" do
          expect(subject.mass_to_fuel(mass)).to eq(expected_fuel)
        end
      end
    end

    context 'would be negative fuel' do
      it 'rounds fuel to 0' do
        expect(subject.mass_to_fuel(2)).to be_zero
      end
    end
  end

  describe '#full_mass_to_fuel' do
    context 'cumulative mass' do
      [
        [14, 2],
        [1969, 966],
        [100_756, 50_346]
      ].each do |mass, expected_fuel|
        it "calculates cumulative fuel for mass #{mass}" do
          expect(subject.cumulative_mass_to_fuel(mass)).to eq(expected_fuel)
        end
      end
    end
  end
end
