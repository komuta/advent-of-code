# frozen_string_literal: true

require_relative '../../lib/intcode/computer.rb'

module Intcode
  describe Computer do
    describe '#run' do
      let(:memory) do
        Memory.read(intcodes)
      end

      let(:subject) do
        Computer.new(memory, input, output)
      end

      let(:input) { StringIO.new }
      let(:output) { StringIO.new }

      [
        ['1,0,0,0,99', '2,0,0,0,99'],
        ['2,3,0,3,99', '2,3,0,6,99'],
        ['2,4,4,5,99,0', '2,4,4,5,99,9801'],
        ['1,1,1,4,99,5,6,0,99', '30,1,1,4,2,5,6,0,99'],
        ['1101,100,-1,4,0', '1101,100,-1,4,99']
      ].each do |intcodes, expected_result|
        context 'finished program' do
          let(:intcodes) { intcodes }
          it 'returns expected result' do
            subject.run
            result = subject.memory.to_s
            expect(result).to eq(expected_result)
          end
        end
      end

      context 'opcode 3' do
        let(:intcodes) { '3,3,99,0' }
        let(:input) { StringIO.new('1') }

        it 'reads input and store it' do
          subject.run
          expect(subject.memory.read(3)).to eq(1)
        end
      end

      context 'opcode 4' do
        let(:intcodes) { '4,3,99,666' }

        it 'reads memory and outputs it' do
          subject.run
          expect(subject.output.string.strip).to eq('666')
        end
      end

      context 'opcode 5 - jump-if-true' do
        let(:intcodes) { '1105,1,4,66,99' }

        it 'jumps and finish the program without error' do
          expect { subject.run }.not_to raise_error
        end
      end

      context 'opcode 6 - jump-if-false' do
        let(:intcodes) { '1106,0,4,66,99' }

        it 'jumps and finish the program without error' do
          expect { subject.run }.not_to raise_error
        end
      end

      context 'opcode 7 - less than' do
        let(:intcodes) { '11107,0,4,5,99,77' }

        it 'stores 1 when 1st parameter is inferior to 2nd' do
          subject.run
          expect(subject.memory.read(5)).to eq(1)
        end
      end

      context 'opcode 7 - equals' do
        let(:intcodes) { '11108,4,4,5,99,77' }

        it 'stores 1 when 1st parameter is equal to 2nd' do
          subject.run
          expect(subject.memory.read(5)).to eq(1)
        end
      end

      context 'parameter mode 1' do
        let(:intcodes) { '1001,5,12,0,99,1' }

        it 'reads immediate value' do
          subject.run
          expect(subject.memory.read(0)).to eq(13)
        end
      end

      context 'unknown code' do
        let(:intcodes) { '666' }

        it 'raises error' do
          expect { subject.run }.to raise_error(/unknown code/)
        end
      end

      context 'equal-to-8 program with input to 8' do
        let(:intcodes) { '3,9,8,9,10,9,4,9,99,-1,8' }
        let(:input) { StringIO.new('8') }

        it 'outputs 1' do
          subject.run
          expect(subject.output.string.strip).to eq('1')
        end
      end

      context 'day 5 example' do
        let(:intcodes) do
          <<~INTCODES
            3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
          INTCODES
        end

        context 'input below 8' do
          let(:input) { StringIO.new('5') }

          it 'outputs 999' do
            subject.run
            expect(subject.output.string.strip).to eq('999')
          end
        end

        context 'input equals 8' do
          let(:input) { StringIO.new('8') }

          it 'outputs 1000' do
            subject.run
            expect(subject.output.string.strip).to eq('1000')
          end
        end

        context 'input above 8' do
          let(:input) { StringIO.new('15') }

          it 'outputs 1001' do
            subject.run
            expect(subject.output.string.strip).to eq('1001')
          end
        end
      end
    end
  end
end
