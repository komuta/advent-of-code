# frozen_string_literal: true

require_relative '../../lib/intcode/amplifier'

module Intcode
  describe AmplifierChain do
    describe '#run_phase_sequence' do
      let(:memory) do
      end

      let(:phase_sequence) do
      end

      [
        ['3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0', '4,3,2,1,0', '43210'],
        ['3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0',
         '0,1,2,3,4', '54321'],
        ['3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0',
         '1,0,4,3,2', '65210']
      ].each do |program, phase_sequence, expected_signal|
        it "outputs amplified signal #{expected_signal}" do
          chain = AmplifierChain.new(Memory.read(program), 5)
          signal = chain.run_phase_sequence(phase_sequence.split(',').map(&:to_i))
          # puts signal
          expect(signal).to eq(expected_signal)
        end
      end
    end
  end
end
