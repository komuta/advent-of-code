# frozen_string_literal: true

require_relative '../lib/astral_object.rb'

describe AstralObjectIndex do
  context '#orbits_count' do
    let(:map) do
      <<~MAP
        COM)B
        B)C
        C)D
        D)E
        E)F
        B)G
        G)H
        D)I
        E)J
        J)K
        K)L
      MAP
    end

    subject do
      AstralObjectIndex.new
    end

    it 'returns total orbits count' do
      subject.parse(map)
      expect(subject.orbits_count).to eq(42)
    end
  end
end

describe AstralObject do
  let(:map) do
    <<~MAP
      COM)B
      B)C
      C)D
      D)E
      E)F
      B)G
      G)H
      D)I
      E)J
      J)K
      K)L
      K)YOU
      I)SAN
    MAP
  end

  let(:index) do
    AstralObjectIndex.new.parse(map)
  end

  describe '#orbits' do
    let(:orbiter) do
      index.object('YOU')
    end

    it 'contains D as orbits' do
      d = index.object('D')
      expect(orbiter.orbits).to include(d)
    end
  end

  describe '#orbital_transfers_between' do
    context 'from YOU to SAN' do
      let(:source) { index.object('YOU') }
      let(:destination) { index.object('SAN') }

      it 'returns 4' do
        expect(index.orbital_transfers_between(source, destination)).to eq(4)
      end
    end
  end
end
