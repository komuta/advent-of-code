# frozen_string_literal: true

require_relative '../lib/wire_path.rb'

describe Point do
  describe '#distance' do
    it 'returns manhattan distance between 2 points' do
      a = Point.new(1, 1)
      b = Point.new(4, 4)
      expect(a.distance_to(b)).to eq(6)
    end

    it 'is commutative' do
      a = Point.new(1, 1)
      b = Point.new(4, 4)
      expect(a.distance_to(b)).to eq(b.distance_to(a))
    end
  end
end

describe Edge do
  subject do
    Edge.read(Point.new(1, 1), 'R5')
  end

  describe '#ending' do
    it 'returns ending point of the edge' do
      expect(subject.ending).to eq(Point.new(6, 1))
    end
  end

  describe '#initialize' do
    context 'misaligned points' do
      subject do
        Edge.new(Point.new(1, 1), Point.new(2, 2))
      end

      it 'raises an error' do
        expect { subject }.to raise_error
      end
    end
  end

  describe '#intersect?' do
    context 'crossing edges' do
      it 'returns true' do
        a = Edge.new(Point.new(1, 1), Point.new(1, 5))
        b = Edge.new(Point.new(2, 2), Point.new(0, 2))
        expect(a.intersect?(b)).to be_truthy
      end
    end
  end

  describe '#intersection' do
    context 'crossing edges' do
      it 'returns the intersection point' do
        a = Edge.new(Point.new(1, 1), Point.new(1, 5))
        b = Edge.new(Point.new(2, 2), Point.new(0, 2))

        expect(a.intersection(b)).to eq(Point.new(1, 2))
      end
    end
  end
end

describe WirePath do
  subject do
    WirePath.read('R8,U5,L5,D3')
  end

  describe '#steps_to' do
    # it 'returns' do
    #   a = WirePath.origin
    #   b = subject.edges.first.ending
    # end
  end

  describe 'examples' do
    [
      ['R8,U5,L5,D3', 'U7,R6,D4,L4', 30],
      ['R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83', 610],
      ['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7', 410]
    ].each do |first, second, optimal|
      context "#{first} x #{second} -> #{optimal}" do
        it 'calculates optimal' do
          first_path = WirePath.read(first)
          second_path = WirePath.read(second)
          result = first_path
                   .intersections(second_path)
                   .map do |intersection|
            first_path.steps_to(intersection) + second_path.steps_to(intersection)
          end.min
          expect(result).to eq(optimal)
        end
      end
    end
  end
end
