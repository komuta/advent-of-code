# frozen_string_literal: true

require_relative '../lib/password.rb'

describe Password do
  describe '#meet_criteria?' do
    %w[112233 111122].each do |password|
      context password do
        subject do
          Password.new(password)
        end

        it 'meets criteria' do
          expect(subject.meet_criteria?).to be_truthy
        end
      end
    end

    %w[abc abc123 223450 123789 123444 111111].each do |password|
      context password do
        subject do
          Password.new(password)
        end

        it "doesn't meet criteria" do
          expect(subject.meet_criteria?).to be_falsey
        end
      end
    end
  end
end
