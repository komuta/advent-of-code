#!env ruby
# frozen_string_literal: true

require_relative 'lib/intcode/amplifier.rb'

memory = Intcode::Memory.read(File.read('day7_input'))
chain = Intcode::AmplifierChain.new(memory, 5)

(0..4)
  .to_a
  .permutation
  .map { |phase_sequence| [chain.run_phase_sequence(phase_sequence).to_i, phase_sequence] }
  .max { |a, b| a.first <=> b.first }
  .tap do |output, phase_sequence|
  puts "max thruster signal #{output} for phase sequence #{phase_sequence}"
end
