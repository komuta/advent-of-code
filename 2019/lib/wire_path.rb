# frozen_string_literal: true

class WirePath
  attr_reader :edges

  # @param edges [Array<Edge>]
  def initialize(edges)
    @edges = edges
  end

  # @return [Point]
  def self.origin
    Point.new(1, 1)
  end

  def horizontal_edges
    @edges.select(&:horizontal?)
  end

  def vertical_edges
    @edges.select(&:vertical?)
  end

  # @param other [WirePath]
  # @return [Array<Point>]
  def intersections(other)
    [
      horizontal_edges.map do |this_edge|
        other.vertical_edges.map do |other_edge|
          this_edge.intersection(other_edge)
        end.reject(&:nil?)
      end.flatten, # rubocop:disable Style/MultilineBlockChain
      vertical_edges.map do |this_edge|
        other.horizontal_edges.map do |other_edge|
          this_edge.intersection(other_edge)
        end.reject(&:nil?)
      end.flatten
    ].flatten
      .reject do |point|
      point == WirePath.origin
    end
  end

  # @param destination [Point]
  # @return [Integer] number of steps
  def steps_to(destination)
    @edges.inject(0) do |steps, edge|
      break steps + Edge.new(edge.start, destination).steps if edge.start.aligned_with?(destination)

      steps + edge.steps
    end
  end

  # @param wire_path [String]
  # @return [WirePath]
  def self.read(wire_path)
    cursor = origin
    edges = wire_path.split(',').map do |direction_length|
      edge = Edge.read(cursor, direction_length)
      cursor = edge.ending
      edge
    end
    WirePath.new(edges)
  end

  def to_s
    [WirePath.origin, @edges.map(&:ending)]
      .flatten
      .map(&:to_s)
      .join(' -> ')
  end
end

class Integer
  def within?(low, high)
    [low, high].min <= self && self <= [low, high].max
  end
end

class NotAnEdgeError
end

class Edge
  attr_reader :start, :ending

  # @param start [Point]
  # @param ending [Point]
  def initialize(start, ending)
    raise NotAnEdgeError unless start.aligned_with?(ending)
    raise NotAnEdgeError if start == ending

    @start = start
    @ending = ending
  end

  # @param start [Point]
  # @param direction_length [String]
  def self.read(start, direction_length)
    length = direction_length.slice(1..-1).to_i
    ending = case direction_length[0]
             when 'L'
               Point.new(start.x - length, start.y)
             when 'R'
               Point.new(start.x + length, start.y)
             when 'U'
               Point.new(start.x, start.y + length)
             when 'D'
               Point.new(start.x, start.y - length)
             else
               raise 'unknown direction'
             end
    Edge.new(start, ending)
  end

  def steps
    start.distance_to(ending)
  end

  def horizontal?
    start.y == ending.y
  end

  def vertical?
    start.x == ending.x
  end

  # @param other [Edge]
  # @return [Boolean]
  def intersect?(other)
    if horizontal?
      other.vertical? &&
        start.y.within?(other.start.y, other.ending.y) &&
        other.start.x.within?(start.x, ending.x)
    else
      other.horizontal? &&
        start.x.within?(other.start.x, other.ending.x) &&
        other.start.y.within?(start.y, ending.y)
    end
  end

  # @param other [Edge]
  # @return [Point]
  def intersection(other)
    return nil unless intersect?(other)

    if horizontal?
      Point.new(other.start.x, start.y)
    else
      Point.new(start.x, other.start.y)
    end
  end

  def to_s
    "#{start} -> #{ending}"
  end
end

class Point
  attr_reader :x, :y

  def initialize(x, y) # rubocop:disable Naming/MethodParameterName
    @x = x
    @y = y
  end

  def ==(other)
    x == other.x && y == other.y
  end

  # Calculate Manhattan distance between 2 points
  def distance_to(other)
    (x - other.x).abs + (y - other.y).abs
  end

  def aligned_with?(other)
    x == other.x || y == other.y
  end

  def to_s
    "(#{x}, #{y})"
  end
end
