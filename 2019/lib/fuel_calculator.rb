# frozen_string_literal: true

# Mass to Fuel calculator
class FuelCalculator
  # @param mass [Integer]
  # @return Integer
  def mass_to_fuel(mass)
    [(mass.to_f / 3).floor - 2, 0].max
  end

  # @param mass [Integer]
  # @return Integer
  def cumulative_mass_to_fuel(mass)
    # FIXME: think better implem
    cumulative_fuel = 0
    while mass_to_fuel(mass).positive?
      fuel = mass_to_fuel(mass)
      cumulative_fuel += fuel
      mass = fuel
    end
    cumulative_fuel
  end
end
