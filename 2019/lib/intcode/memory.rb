# frozen_string_literal: true

module Intcode
  class Memory
    # @param intcodes [Array<Integer>]
    def initialize(intcodes)
      @data = intcodes
    end

    def clone
      Memory.new(@data.clone)
    end

    # @param serialized_memory [String] Intcode memory string representation
    def self.read(serialized_memory)
      intcodes = serialized_memory.split(',').map(&:to_i)
      Memory.new(intcodes)
    end

    # @param address [Integer]
    # @return [Integer]
    def read(address)
      @data[address]
    end

    # @param address [Integer]
    # @return [Integer]
    def read_at(address)
      value_address = read(address)
      read(value_address)
    end

    # @param address [Integer]
    # @param value [Integer]
    def write(address, value)
      raise 'not an integer' unless value.is_a?(Integer)

      @data[address] = value
    end

    # @param address [Integer]
    # @param value [Integer]
    def write_at(address, value)
      value_address = read(address)
      write(value_address, value)
    end

    def to_s
      @data.join(',')
    end
  end
end
