# frozen_string_literal: true

require_relative 'memory'

module Intcode
  class Computer
    attr_reader :memory, :input, :output
    attr_accessor :debug

    OPCODES = %i[
      null
      add multiply
      read_input write_output
      jump_if_true jump_if_false
      less_than equals
    ].freeze

    # @param memory [Memory]
    # @param input [IO]
    # @param output [IO]
    def initialize(memory, input, output)
      @memory = memory
      @input = input
      @output = output
      reset
    end

    def reset
      @instruction_pointer = 0
      @current_instruction = 0
    end

    def run
      loop do
        opcode = read_instruction
        if (1..OPCODES.size - 1).include?(opcode)
          puts "exec #{OPCODES[opcode]}" if debug
          send(OPCODES[opcode])
        elsif opcode == 99
          break
        else
          raise "unknown code #{opcode}"
        end
      end
    end

    private

    def read_intcode
      intcode = memory.read(@instruction_pointer)
      @instruction_pointer += 1
      intcode
    end

    def read_instruction
      instruction = read_intcode
      @parameter_modes = instruction / 100
      @current_instruction = instruction % 100
    end

    def read_parameter
      parameter_mode = @parameter_modes % 10
      @parameter_modes /= 10
      case parameter_mode
      when 0
        address = read_intcode
        memory.read(address)
      when 1
        read_intcode
      else
        raise "unknown parameter mode #{parameter_mode}"
      end
    end

    def add
      a = read_parameter
      b = read_parameter
      address = read_intcode
      memory.write(address, a + b)
    end

    def multiply
      a = read_parameter
      b = read_parameter
      address = read_intcode
      memory.write(address, a * b)
    end

    def read_input
      value = input.readline.strip.to_i
      address = read_intcode
      memory.write(address, value)
      puts "read #{value}" if debug
    end

    def write_output
      value = read_parameter
      output.puts(value)
      puts "write #{value}" if debug
    end

    def jump_if_true
      test = read_parameter
      address = read_parameter
      @instruction_pointer = address unless test.zero?
    end

    def jump_if_false
      test = read_parameter
      address = read_parameter
      @instruction_pointer = address if test.zero?
    end

    def less_than
      a = read_parameter
      b = read_parameter
      address = read_intcode
      result = a < b ? 1 : 0
      memory.write(address, result)
    end

    def equals
      a = read_parameter
      b = read_parameter
      address = read_intcode
      result = a == b ? 1 : 0
      memory.write(address, result)
      puts "#{a} equals #{b}? #{result} -> #{address}" if debug
    end
  end
end
