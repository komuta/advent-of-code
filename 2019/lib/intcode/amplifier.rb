# frozen_string_literal: true

require_relative 'computer'
require_relative 'memory'

module Intcode
  class Amplifier
    def initialize(memory)
      @memory = memory
    end

    # @param phase [Integer]
    # @param input_signal [Integer]
    # @return [Integer] output signal
    def run(phase, input_signal)
      input = StringIO.new
      output = StringIO.new
      input.puts(phase)
      input.puts(input_signal)
      input.rewind
      computer = Computer.new(@memory.clone, input, output)
      # computer.debug = true
      computer.run
      output.string.strip
    end
  end

  class AmplifierChain
    # @param program [Memory]
    # @param size [Intcode]
    def initialize(program, size)
      @amplifiers = Array.new(size) { Amplifier.new(program) }
    end

    # @param phase_sequence [Array<Integer>]
    # @return [String]
    def run_phase_sequence(phase_sequence)
      phases = phase_sequence.clone
      # signal = []
      @amplifiers.inject(0) do |input_signal, amplifier|
        # output_signal = amplifier.run(phases.shift, input_signal)
        # signal << output_signal
        # output_signal
        amplifier.run(phases.shift, input_signal)
      end
      # signal
    end
  end
end
