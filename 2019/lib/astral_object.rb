# frozen_string_literal: true

class AstralObject
  attr_reader :name
  attr_reader :direct_orbits

  # @param name [String]
  def initialize(name)
    @name = name
    @direct_orbits = []
  end

  # @param object [AstralObject]
  def add_direct_orbit(object)
    @direct_orbits << object
  end

  # @return Integer
  def orbits_count
    direct_orbits_count + indirect_orbits_count
  end

  # @return Array<AstralObject>
  def orbits
    @orbits ||= [@direct_orbits, @direct_orbits.map(&:orbits)].flatten
  end

  # @param object AstralObject
  # @return Integer
  def orbital_tranfer_to(object)
    raise 'not an orbit' unless orbits.include?(object)

    return 0 if @direct_orbits.include?(object)

    1 + (@direct_orbits
        .select { |orbit| orbit.orbits.include?(object) }
        .map { |orbit| orbit.orbital_tranfer_to(object) }
        .min || 0)
  end

  private

  # @return Integer
  def direct_orbits_count
    @direct_orbits.size
  end

  # @return Integer
  def indirect_orbits_count
    @direct_orbits.inject(0) { |count, object| count + object.orbits_count }
  end

  def to_s
    name
  end
end

class AstralObjectIndex
  def initialize
    @objects = {}
  end

  # @return Integer
  def orbits_count
    @objects.values.inject(0) { |count, object| count + object.orbits_count }
  end

  # @param name [String]
  # @return AstralObject,nil
  def object(name)
    @objects[name]
  end

  # @return Integer
  def orbital_transfers_between(source, destination)
    (source.orbits & destination.orbits).map do |intersection|
      # puts "#{source} -> #{intersection} = #{source.orbital_tranfer_to(intersection)}"
      # puts "#{destination} -> #{intersection} = #{destination.orbital_tranfer_to(intersection)}"
      source.orbital_tranfer_to(intersection) + destination.orbital_tranfer_to(intersection)
    end.min
  end

  # @param map [String]
  def parse(map)
    map.split("\n")
       .each do |line|
      line.split(')').tap do |first, second|
        @objects[first] = AstralObject.new(first) unless @objects.key?(first)
        @objects[second] = AstralObject.new(second) unless @objects.key?(second)

        orbited = @objects[first]
        orbiter = @objects[second]

        orbiter.add_direct_orbit(orbited)
      end
    end
    self
  end
end
