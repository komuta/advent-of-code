# frozen_string_literal: true

class Password
  # @param password [String]
  def initialize(password)
    @password = password
  end

  def meet_criteria?
    six_digit_number? &&
      strict_two_identical_adjacent_digits? &&
      constains_increasing_numbers?
  end

  private

  def six_digit_number?
    @password.match(/^\d{6}$/)
  end

  def constains_increasing_numbers?
    @password.each_char
             .each_cons(2) { |a, b| return false if b < a }
    true
  end

  def strict_two_identical_adjacent_digits?
    adjacent_siblings_count = []
    c = 1
    @password.each_char
             .each_cons(2) do |a, b|
      c = a == b ? c + 1 : 1
      adjacent_siblings_count << c
    end
    adjacent_siblings_count.each_cons(2) do |a, b|
      return true if a == 2 && b != 3
    end
    adjacent_siblings_count.last == 2
  end
end
