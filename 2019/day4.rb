#!env ruby
# frozen_string_literal: true

require_relative 'lib/password.rb'

range = (356_261..846_303)

puts range
  .map { |password| Password.new(password.to_s) }
  .select(&:meet_criteria?)
  .count

# part 1 correct answer is 544
# part 2 correct answer is 334
