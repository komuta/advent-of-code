#!env ruby
# frozen_string_literal: true

require_relative 'lib/fuel_calculator.rb'

calculator = FuelCalculator.new

modules_mass = File.read('day1_input')
                   .split("\n")
                   .map(&:to_i)

total_fuel = modules_mass.inject(0) do |fuel, module_mass|
  fuel + calculator.mass_to_fuel(module_mass)
end
puts "Sum of modules fuel requirements: #{total_fuel}"

# good answer: 3270338

cumulative_fuel = modules_mass.inject(0) do |fuel, module_mass|
  fuel + calculator.cumulative_mass_to_fuel(module_mass)
end
puts "Cumulative sum of modules fuel requirements: #{cumulative_fuel}"

# good answer: 4902650
