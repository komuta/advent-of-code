# frozen_string_literal: true

require_relative '../lib/area_map'

describe Area::Map do
  let(:day3_example) do
    <<~EXAMPLE.split("\n")
      ..##.......
      #...#...#..
      .#....#..#.
      ..#.#...#.#
      .#...##..#.
      ..#.##.....
      .#.#.#....#
      .#........#
      #.##...#...
      #...##....#
      .#..#...#.#
    EXAMPLE
  end

  describe '#walk_slope' do
    it 'allows to count the 🎄' do
      map = Area::Map.new(day3_example)
      puts map
      tree_count = map.walk_slope(right: 3, down: 1).count(&:tree?)
      expect(tree_count).to eq(7)
    end
  end
end
