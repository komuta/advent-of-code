require_relative '../lib/adapter'

describe AdapterArray do
  subject do
    AdapterArray.new(adapters)
  end

  simple_example = [
    'simple example',
    [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4],
    { 1 => 7, 3 => 5 },
    8
  ]
  larger_example = [
    'larger example',
    [28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3],
    { 1 => 22, 3 => 10 },
    19208
  ]
  describe '#joltage_distribution' do
    [simple_example, larger_example].each do |label, adapters, expected_distribution|
      context label do
        let(:adapters) do
          adapters
        end
        let(:expected_distribution) do
          expected_distribution
        end

        it 'returns the expected distribution' do
          expect(subject.joltage_distribution).to eq(expected_distribution)
        end
      end
    end
  end

  describe '#count_arrangements' do
    [simple_example, larger_example].each do |label, adapters, _, expected_count|
      context label do
        let(:adapters) do
          adapters
        end
        it 'computes the expected arragements count' do
          expect(subject.count_arrangements).to eq(expected_count)
        end
      end
    end
  end
end
