require_relative '../lib/custom_form'

describe CustomForm::GroupAnswers do
  subject do
    CustomForm::GroupAnswers.new(answers)
  end

  describe '#yes_count' do
    [
      ['abc', 3],
      ["a\nb\nc", 3],
      ["ab\nac", 3],
      ["a\na\na\na", 1],
      ['b', 1]
    ].each do |answers, yes_count|
      context "group answers #{answers.tr("\n", '.')}" do
        let(:answers) { answers }

        it 'returns the count of positive answers' do
          expect(subject.yes_count).to eq(yes_count)
        end
      end
    end
  end

  describe '#all_yes_count' do
    [
      ['abc', 3],
      ["a\nb\nc", 0],
      ["ab\nac", 1],
      ["a\na\na\na", 1],
      ['b', 1]
    ].each do |answers, yes_count|
      context "group answers #{answers.tr("\n", '.')}" do
        let(:answers) { answers }

        it 'returns the count of positive answers' do
          expect(subject.all_yes_count).to eq(yes_count)
        end
      end
    end
  end
end
