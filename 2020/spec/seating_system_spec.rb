require_relative '../lib/seating_system'
require 'super_diff/rspec'

describe SeatingSystem do
  subject do
    SeatingSystem.new(seats_map)
  end

  seats_map = <<~SEATS
    L.LL.LL.LL
    LLLLLLL.LL
    L.L.L..L..
    LLLL.LL.LL
    L.LL.LL.LL
    L.LLLLL.LL
    ..L.L.....
    LLLLLLLLLL
    L.LLLLLL.L
    L.LLLLL.LL
  SEATS

  describe '#run_round' do
    let(:seats_map) { seats_map }
    [
      [<<~SEATS, 1],
        #.##.##.##
        #######.##
        #.#.#..#..
        ####.##.##
        #.##.##.##
        #.#####.##
        ..#.#.....
        ##########
        #.######.#
        #.#####.##
      SEATS
      [<<~SEATS, 2]
        #.LL.L#.##
        #LLLLLL.L#
        L.L.L..L..
        #LLL.LL.L#
        #.LL.LL.LL
        #.LLLL#.##
        ..L.L.....
        #LLLLLLLL#
        #.LLLLLL.L
        #.#LLLL.##
      SEATS
    ].each do |map, rounds_count|
      context "after #{rounds_count} rounds" do
        let(:expected_map) do
          map.split("\n")
        end

        it 'update the seats' do
          rounds_count.times { subject.run_round }
          expect(subject.seats_map).to eq(expected_map)
        end
      end
    end
  end

  describe '#stabilize' do
    let(:seats_map) { seats_map }
    let(:expected_map) do
      <<~SEATS.split("\n")
        #.#L.L#.##
        #LLL#LL.L#
        L.#.L..#..
        #L##.##.L#
        #.#L.LL.LL
        #.#L#L#.##
        ..L.L.....
        #L#L##L#L#
        #.LLLLLL.L
        #.#L#L#.##
      SEATS
    end

    it 'update the seats' do
      subject.stabilize
      expect(subject.seats_map).to eq(expected_map)
    end

    let(:occupied_seats) { 37 }
    it "counts occupied seats" do
      subject.stabilize
      expect(subject.occupied_seats).to eq(occupied_seats)
    end
  end
end
