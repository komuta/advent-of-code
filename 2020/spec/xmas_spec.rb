require_relative '../lib/xmas'

describe Xmas::Checker do
  subject do
    Xmas::Checker.new(preamble_size)
  end

  let(:preamble_size) { 25 }

  describe '#check_until_invalid' do
    shared_examples 'valid data' do
      it 'returns nil' do
        expect(subject.check_until_invalid(data)).to be_nil
      end
    end

    shared_examples 'invalid data' do |invalid_value|
      it 'returns the first invalid value encountered' do
        expect(subject.check_until_invalid(data)).to eq(invalid_value)
      end
    end

    context 'preamble with 1 to 25 in random order' do
      let(:preamble) do
        (1..25).to_a.shuffle
      end

      [
        [26, true],
        [49, true],
        [100, false],
        [50, false]
      ].each do |next_number, is_valid|
        context "#{next_number} is the next number" do
          let(:data) { preamble + [next_number] }

          it_behaves_like "#{is_valid ? 'valid' : 'invalid'} data", next_number
        end
      end
    end

    context 'preamble is 20, 1 to 25 without 20 in random order, and 45' do
      let(:preamble) do
        (1..25).to_a.shuffle.tap do |data|
          data.delete(20)
          data.unshift(20)
          data.append(45)
        end
      end

      [
        [26, true],
        [65, false],
        [64, true],
        [66, true]
      ].each do |next_number, is_valid|
        context "#{next_number} is the next number" do
          let(:data) { preamble + [next_number] }

          it_behaves_like "#{is_valid ? 'valid' : 'invalid'} data", next_number
        end
      end
    end

    context 'longer example with preamble of length 5' do
      let(:preamble_size) { 5 }
      let(:data) do
        [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576]
      end

      it_behaves_like 'invalid data', 127
    end
  end

  describe '#find_contiguous_set_of_sum' do
    let(:preamble_size) { 5 }
    let(:data) do
      [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576]
    end

    it 'returns the first contiguous set matching the sum' do
      expect(subject.find_contiguous_set_of_sum(data, 127)).to eq([15, 25, 47, 40])
    end
  end
end
