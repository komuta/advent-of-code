require_relative '../lib/bag_rules'

bag_rules = <<~RULES.split("\n").map(&BagRule.method(:parse))
  light red bags contain 1 bright white bag, 2 muted yellow bags.
  dark orange bags contain 3 bright white bags, 4 muted yellow bags.
  bright white bags contain 1 shiny gold bag.
  muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
  shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
  dark olive bags contain 3 faded blue bags, 4 dotted black bags.
  vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
  faded blue bags contain no other bags.
  dotted black bags contain no other bags.
RULES

describe BagRule do
  subject do
    BagRule.parse(bag_rule)
  end

  describe '.parse' do
    context 'empty bag rule' do
      let(:bag_rule) { 'faded blue bags contain no other bags.' }

      it 'returns an empty bag' do
        expect(subject).to be_empty
      end
    end

    context 'full bag rule' do
      let(:bag_rule) { 'light red bags contain 1 bright white bag, 2 muted yellow bags.' }

      it 'returns a full bag' do
        expect(subject).not_to be_empty
      end
    end
  end
end

describe BagFinder do
  subject do
    BagFinder.new(target)
  end

  describe '#find_in' do
    let(:target) { 'shiny gold' }

    it 'extracts all bags containing the target' do
      expect(subject.find_in(bag_rules).size).to eq(4)
    end
  end
end

describe BagTree do
  subject do
    BagTree.new(target, bag_rules)
  end

  describe '#count' do
    let(:target) { 'shiny gold' }

    it 'returns total count of contained bags' do
      expect(subject.count).to eq(33)
    end
  end
end
