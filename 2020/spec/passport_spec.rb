require_relative '../lib/passport'

describe Passport do
  passports = <<~PASSPORTS.split("\n\n")
    ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
    byr:1937 iyr:2017 cid:147 hgt:183cm

    iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
    hcl:#cfa07d byr:1929

    hcl:#ae17e1 iyr:2013
    eyr:2024
    ecl:brn pid:760753108 byr:1931
    hgt:179cm

    hcl:#cfa07d eyr:2025 pid:166559648
    iyr:2011 ecl:brn hgt:59in
  PASSPORTS

  subject do
    Passport.new(serialized_passport)
  end

  describe '#valid?' do
    shared_examples 'valid passport' do
      it 'returns true' do
        expect(subject).to be_valid
      end
    end

    shared_examples 'invalid passport' do
      it 'returns false' do
        expect(subject).not_to be_valid
      end
    end

    context 'valid passport' do
      let(:serialized_passport) do
        <<~PASSPORT
          ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
          byr:1937 iyr:2017 cid:147 hgt:183cm
        PASSPORT
      end

      it_behaves_like 'valid passport'
    end

    context 'invalid passport' do
      let(:serialized_passport) do
        <<~PASSPORT
          iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
          hcl:#cfa07d byr:1929
        PASSPORT
      end

      it_behaves_like 'invalid passport'
    end

    context 'passport missiong cid' do
      let(:serialized_passport) do
        <<~PASSPORT
          hcl:#ae17e1 iyr:2013
          eyr:2024
          ecl:brn pid:760753108 byr:1931
          hgt:179cm
        PASSPORT
      end

      it_behaves_like 'valid passport'
    end

    context 'passport missing two fields' do
      let(:serialized_passport) do
        <<~PASSPORT
          hcl:#cfa07d eyr:2025 pid:166559648
          iyr:2011 ecl:brn hgt:59in
        PASSPORT
      end

      it_behaves_like 'invalid passport'
    end
  end

  describe '#valid_field?' do
    shared_examples 'valid field' do |field, value|
      it 'returns true' do
        expect(Passport.valid_field?(field, value)).to be_truthy
      end
    end

    shared_examples 'invalid field' do |field, value|
      it 'returns false' do
        expect(Passport.valid_field?(field, value)).to be_falsey
      end
    end

    context 'valid byr' do
      it_behaves_like 'valid field', 'byr', '2002'
    end

    context 'invalid byr' do
      it_behaves_like 'invalid field', 'byr', '2003'
    end

    context 'invalid hcl' do
      it_behaves_like 'invalid field', 'hcl', '123abc'
    end
  end
end
