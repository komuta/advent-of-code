require_relative '../lib/computer'

program = <<~PROGRAM.split("\n")
  nop +0
  acc +1
  jmp +4
  acc +3
  jmp -3
  acc -99
  acc +1
  jmp -4
  acc +6
PROGRAM

describe Computer do
  subject do
    Computer.new(program)
  end

  describe '#run_until_visited' do
    it 'stops at the first already visited instruction' do
      subject.run_until_visited
      expect(subject.next_instruction).to eq('acc +1')
    end
  end
end

describe ProgramFixer do
  subject do
    ProgramFixer.new(program)
  end

  describe '#fix' do
    it 'fixes the program' do
      fixed_program = subject.fix
      expect(fixed_program).not_to be_nil
    end

    it 'returns a program which terminates' do
      fixed_program = subject.fix
      expect(Computer.new(fixed_program).run_until_visited.accumulator).to eq(8)
    end
  end
end
