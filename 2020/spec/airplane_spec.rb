require_relative '../lib/airplane'

describe Airplane::Seat do
  subject do
    Airplane::Seat.new(boarding_pass)
  end

  describe '#initialize' do
    [
      ['FBFBBFFRLR', 44, 5, 357],
      ['BFFFBBFRRR', 70, 7, 567],
      ['FFFBBBFRRR', 14, 7, 119],
      ['BBFFBBFRLL', 102, 4, 820]
    ].each do |boarding_pass, expected_row, expected_column, expected_seat_id|
      context "seat #{boarding_pass}" do
        let(:boarding_pass) { boarding_pass }

        it "computes expected row #{expected_row}" do
          expect(subject.row).to eq(expected_row)
        end

        it "computes expected column #{expected_column}" do
          expect(subject.column).to eq(expected_column)
        end

        it "computes expected seat_id #{expected_seat_id}" do
          expect(subject.seat_id).to eq(expected_seat_id)
        end
      end
    end
  end
end
