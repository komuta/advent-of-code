#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/custom_form'

group_answers = File.read('day6_input').split("\n\n")
                    .map(&CustomForm::GroupAnswers.method(:new))

# Part 1
yes_answers_sum = group_answers.map(&:yes_count).sum
puts "Part 1: #{yes_answers_sum}"

# Part 2
all_yes_answers_sum = group_answers.map(&:all_yes_count).sum
puts "Part 2: #{all_yes_answers_sum}"
