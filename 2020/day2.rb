#!env ruby
# frozen_string_literal: true

require 'pry'

class RangePasswordPolicy
  attr_reader :min, :max, :char

  def initialize(rule)
    captures = /(?<min>\d+)-(?<max>\d+) (?<char>\w)/.match(rule).named_captures
    @min = captures['min'].to_i
    @max = captures['max'].to_i
    @char = captures['char']
  end

  def satisfy?(password)
    password.count(char).between?(min, max)
  end
end

class PositionPasswordPolicy
  attr_reader :first, :second, :char

  def initialize(rule)
    captures = /(?<first>\d+)-(?<second>\d+) (?<char>\w)/.match(rule).named_captures
    @first = captures['first'].to_i
    @second = captures['second'].to_i
    @char = captures['char']
  end

  def satisfy?(password)
    [
      password[first - 1],
      password[second - 1]
    ].count do |this_char|
      this_char == char
    end == 1
  end
end

passwords = File.read('day2_input').split("\n")

# Part 1
count = passwords.count do |line|
  rule, password = line.split(/\s*:\s*/)
  RangePasswordPolicy.new(rule).satisfy?(password)
end
puts "Part 1: #{count}"

# Part 2
count = passwords.count do |line|
  rule, password = line.split(/\s*:\s*/)
  PositionPasswordPolicy.new(rule).satisfy?(password)
end
puts "Part 2: #{count}"
