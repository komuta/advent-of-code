#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/airplane'

seats = File.read('day5_input').split("\n").map(&Airplane::Seat.method(:new))

# Part 1
highest_seat_id = seats.map(&:seat_id).max
puts "Part 1: #{highest_seat_id}"

# Part 2
non_listed_seat_ids = Airplane::Seat.all_seat_ids - seats.map(&:seat_id)
non_listed_seat_ids.reject! do |seat_id|
  [0, 127].include?(seat_id / 8)
end
non_listed_seat_ids.reject! do |seat_id|
  non_listed_seat_ids.include?(seat_id - 1) ||
    non_listed_seat_ids.include?(seat_id + 1)
end
puts non_listed_seat_ids
