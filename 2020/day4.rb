#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/passport'

serialized_passports = File.read('day4_input').split("\n\n")
passports = serialized_passports.map(&Passport.method(:new))

# Part 1
count = passports.count(&:valid?)
puts "Part 1: #{count}"

# Part 2
