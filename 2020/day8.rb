#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/computer'

program = File.read('day8_input').split("\n")

# Part 1
computer = Computer.new(program)
computer.run_until_visited
puts "Part 1: accumulator=#{computer.accumulator}"

# Part 2
computer = Computer.new(ProgramFixer.new(program).fix)
computer.run_until_visited
puts "Part 2: accumulator=#{computer.accumulator}"
