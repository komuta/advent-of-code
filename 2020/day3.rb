#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/area_map'

area_lines = File.read('day3_input').split("\n")
area_map = Area::Map.new(area_lines)
puts area_map

# Part 1
count = area_map.walk_slope(right: 3, down: 1)
                .count(&:tree?)
puts "Part 1: #{count}"

# Part 2
count = [
  [1, 1],
  [3, 1],
  [5, 1],
  [7, 1],
  [1, 2]
].map do |right, down|
  area_map.walk_slope(right: right, down: down).count(&:tree?)
end.inject(1, &:*)
puts "Part 2: #{count}"
