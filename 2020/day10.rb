#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/adapter'

adapters = File.read('day10_input').split("\n").map(&:to_i)
adapter_array = AdapterArray.new(adapters)

# Part 1
distribution = adapter_array.joltage_distribution
value = distribution[1] * distribution[3]
puts "Part 1: #{value}"

# Part 2
count = adapter_array.count_arrangements
puts "Part 2: #{count}"
