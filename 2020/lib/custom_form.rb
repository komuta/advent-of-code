module CustomForm
  class GroupAnswers
    def initialize(answers)
      @answers = answers.split("\n")
    end

    def yes_answers
      @answers.flatten
              .join
              .delete("\n")
              .each_char
              .uniq
    end

    def yes_count
      yes_answers.count
    end

    def all_yes_answers
      @answers.map(&:chars)
              .reduce(&:intersection)
    end

    def all_yes_count
      all_yes_answers.count
    end
  end
end
