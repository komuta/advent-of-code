module Xmas
  class Checker
    attr_reader :preamble_size
    attr_reader :debug

    def initialize(preamble_size)
      @preamble_size = preamble_size
    end

    def with_debug
      @debug = true
    end

    # @param data [Array<Integer>]
    # @return [NilClass] when the whole data is valid
    # @return [Integer] value of the first invalid data encountered
    def check_until_invalid(data)
      @possible_sums = {}
      data.each_with_index do |value, current_index|
        puts "#{current_index} -> #{value}" if debug
        unless current_index < preamble_size
          puts "checking #{current_index} -> #{value}" if debug
          index_validity_limit = current_index - preamble_size
          @possible_sums[value]&.reject! do |first, second|
            first < index_validity_limit || second < index_validity_limit
          end
          return value unless @possible_sums.key?(value) && @possible_sums[value].any?
        end
        candidates_start_index = [0, current_index - preamble_size].max
        candidates_end_index = [0, current_index - 1].max
        (candidates_start_index..candidates_end_index).each do |candidate_index|
          next if data[candidate_index] == value

          sum = data[candidate_index] + value
          @possible_sums[sum] ||= []
          @possible_sums[sum] << [candidate_index, current_index]
        end
      end
      # p @possible_sums
      nil
    end

    def find_contiguous_set_of_sum(data, sum)
      data.each_index do |start_index|
        data[start_index..-1].each_with_object([]) do |value, candidate|
          candidate << value
          return candidate if candidate.sum == sum
        end
      end
      nil
    end
  end
end
