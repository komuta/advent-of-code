require 'colorize'

class SeatingSystem
  attr_reader :seats_map

  def initialize(seats_map)
    @seats_map = seats_map.split("\n")
  end

  def stabilize
    loop do
      previous_map = seats_map.join('@')
      run_round
      break if seats_map.join('@') == previous_map
    end
  end

  def run_round
    # new_seats_map = @seats_map.map(&:itself)
    new_seats_map = @seats_map.join('@').split('@') # FIXME: implement a better deepdup
    walk_seats(new_seats_map) do |seat, neighbors|
      if seat == 'L' && neighbors.count('#').zero?
        '#'
      elsif seat == '#' && neighbors.count('#') >= 4
        'L'
      end
    end
    @seats_map = new_seats_map
    # puts @seats_map.join("\n").red
    # puts new_seats_map.join("\n").yellow
  end

  def walk_seats(seats_map = @seats_map)
    seats_map.each_with_index do |seat_row, row|
      seat_row.each_char.each_with_index do |seat, column|
        next if seat == '.'

        update = yield seat, neighbors(row, column)
        seats_map[row][column] = update unless update.nil?
      end
    end
  end

  def neighbors(row, column)
    [
      seat_at(row - 1, column - 1),
      seat_at(row - 1, column),
      seat_at(row - 1, column + 1),
      seat_at(row, column - 1),
      seat_at(row, column + 1),
      seat_at(row + 1, column - 1),
      seat_at(row + 1, column),
      seat_at(row + 1, column + 1)
    ].compact
  end

  # def update_seat_at(seats_map, row, column, seat)
  #   return nil unless row.between?(0, rows_count - 1)
  #   return nil unless column.between?(0, columns_count - 1)

  #   seats_map[row][column] = seat
  # end

  def seat_at(row, column)
    return nil unless row.between?(0, rows_count - 1)
    return nil unless column.between?(0, columns_count - 1)

    @seats_map[row][column]
  end

  def rows_count
    @rows_count ||= @seats_map.size
  end

  def columns_count
    @columns_count ||= @seats_map.first.size
  end

  def occupied_seats
    seats_map.join('').count('#')
  end

  def to_s
    @seats_map.join("\n")
  end
end
