require 'set'

class Passport
  attr_reader :fields

  def initialize(serialized_fields)
    @fields = parse_fields(serialized_fields)
  end

  def valid?
    @fields.keys.to_set & mandatory_fields == mandatory_fields &&
      mandatory_fields.all? do |field|
        Passport.valid_field?(field, @fields[field])
      end
  end

  def self.valid_field?(field, value)
    case field
    when 'byr'
      value.match?(/^\d{4}$/) && value.to_i.between?(1920, 2002)
    when 'iyr'
      value.match?(/^\d{4}$/) && value.to_i.between?(2010, 2020)
    when 'eyr'
      value.match?(/^\d{4}$/) && value.to_i.between?(2020, 2030)
    when 'hgt'
      valid_height?(value)
    when 'hcl'
      value.match?(/^#[0-9a-f]{6}$/)
    when 'ecl'
      %w[amb blu brn gry grn hzl oth].include?(value)
    when 'pid'
      value.match?(/^\d{9}$/)
    else
      raise "unsupported field #{field}"
    end
  end

  def self.valid_height?(height)
    match_data = /^(\d+)(in|cm)$/.match(height)
    return false if match_data.nil?

    case match_data[2]
    when 'in'
      match_data[1].to_i.between?(59, 76)
    when 'cm'
      match_data[1].to_i.between?(150, 193)
    else
      raise "unsupported height '#{height}'"
    end
  end

  private

  def mandatory_fields
    %w[byr iyr eyr hgt hcl ecl pid].to_set
  end

  def parse_fields(serialized_fields)
    # puts serialized_fields
    serialized_fields.gsub("\n", ' ')
                     .split(' ')
                     .map { |field| field.split(':') }
                     .to_h
  end
end
