require 'Forwardable'

class AdapterArray
  def initialize(adapters)
    @adapters = adapters
  end

  def joltage_distribution
    joltage_diffs = adapters_sequence.each_cons(2).map do |first, second|
      second - first
    end
    # p joltage_diffs

    raise "invalid joltage difference: #{first} -> #{second}" unless joltage_diffs.all? do |joltage_diff|
      joltage_diff.between?(1, 3)
    end

    # puts joltage_diffs.tally
    joltage_diffs.tally
  end

  def adapters_sequence
    sequence = @adapters.sort
    sequence.unshift(0)
    sequence << device_joltage
    sequence
  end

  def device_joltage
    @adapters.max + 3
  end

  def count_arrangements
    subsequences = []
    current_subsequence = []
    adapters_sequence.each_cons(2) do |first, second|
      current_subsequence << first
      next unless second - first >= 3

      # puts "split at #{first} - #{second}"
      subsequences << current_subsequence
      current_subsequence = []
    end
    subsequences
      .reject { |subsequence| subsequence.size < 3 }
      .map(&Subsequence.method(:new))
      .map(&:count_arrangements)
      .reduce(:*)
  end

  class Subsequence
    extend Forwardable

    def_delegators :@adapters, :to_s, :inspect

    def initialize(adapters)
      @adapters = adapters.sort
      @@cheat_table ||= {}
    end

    def count_arrangements
      cheat_table_key = @adapters.size
      return @@cheat_table[cheat_table_key] if @@cheat_table.key?(cheat_table_key)

      # puts "count for #{@adapters}"
      count = 0
      @adapters.each_with_object([@adapters.first]) do |_value, tree_row|
        tree_row
          .map! { |value| Range.new(value + 1, value + 3).to_a }
          .flatten!
          .reject! { |value| value > @adapters.last }
        # puts "#{_value} -> #{tree_row}"
        count += tree_row.count(@adapters.last)
        tree_row
      end
      @@cheat_table[cheat_table_key] = count
    end
  end
end
