require 'forwardable'
require 'set'
require 'json'

class BagRule
  attr_reader :type, :contents

  extend Forwardable

  def_delegator :@contents, :empty?

  def initialize(type:, contents: {})
    @type = type
    @contents = contents
  end

  def contains_directly
    @contains_directly ||= @contents.keys.to_set
  end

  def contains_directly?(type)
    @contents.keys.include?(type)
  end

  def self.parse(rule)
    case rule
    when /^(.*) bags contain no other bags\.$/
      BagRule.new(type: Regexp.last_match[1])
    when /^(.*) bags contain (.+)\.$/
      BagRule.new(type: Regexp.last_match[1], contents: parse_contents(Regexp.last_match[2]))
    else
      raise 'bag rule parse error'
    end
  end

  def self.parse_contents(contents)
    contents.split(', ').map do |bag_type_and_quantity|
      /^(\d+) (.+) bags?$/.match(bag_type_and_quantity) do |match|
        [match[2], match[1].to_i]
      end
    end.to_h
  end
end

class BagFinder
  def initialize(target)
    @target = target
  end

  def find_in(bags)
    is_contained_by = bags.each_with_object({}) do |container, map|
      container.contains_directly.each do |bag_type|
        map[bag_type] ||= Set.new
        map[bag_type] << container.type
      end
    end

    containing = bags.select do |bag|
      bag.contains_directly?(@target)
    end.map(&:type).to_set
    next_pass = containing
    loop do
      new_containing = next_pass.map { |bag| is_contained_by[bag].to_a }.compact.flatten.uniq
      break if new_containing.empty?

      containing.merge(new_containing)
      next_pass = new_containing
    end
    containing
  end
end

class BagTree
  class Leaf
    attr_reader :factor

    def initialize(type, bag_index, factor: 1)
      @bag = bag_index[type]
      @factor = factor
      @children = @bag.contents.map do |bag_type, count|
        Leaf.new(bag_type, bag_index, factor: count)
      end
    end

    def count
      @factor * (1 + @children.map(&:count).sum)
    end

    def to_s
      "#{factor} * #{@bag.type}: (#{@children.join(', ')})"
    end
  end

  def initialize(bag_type, bags)
    @bags_index = bags.map { |bag| [bag.type, bag] }.to_h
    @root = Leaf.new(bag_type, @bags_index)
  end

  def count
    @root.count
  end

  def to_s
    @root.to_s
  end
end
