require 'set'

class Computer
  attr_reader :accumulator
  attr_reader :run_sequence

  def initialize(program)
    @accumulator = 0
    @instructions = program
  end

  def run_until_visited
    @cursor = 0
    @visited = Set.new
    @run_sequence = []
    execute_next_instruction until @visited.include?(@cursor) || program_terminated?
    self
  end

  def next_instruction
    @instructions[@cursor]
  end

  def fix_program; end

  def program_terminated?
    @cursor == @instructions.size
  end

  private

  def method_missing(symbol, *_args)
    puts "unknown instruction '#{symbol}'"
  end

  def execute_next_instruction
    # puts next_instruction
    operation, argument = next_instruction.split(' ')
    @visited << @cursor
    @run_sequence << @cursor
    send(operation.to_sym, argument.to_i)
  end

  def nop(_value)
    @cursor += 1
  end

  def acc(value)
    @accumulator += value
    @cursor += 1
  end

  def jmp(offset)
    @cursor += offset
  end
end

class ProgramFixer
  def initialize(program)
    @program = program
  end

  def fix
    computer = Computer.new(@program)
    computer.run_until_visited
    computer.run_sequence.reverse.each do |cursor|
      instruction = @program[cursor]
      next unless instruction =~ /^(nop|jmp)/

      new_program = fix_program_at(cursor)
      return new_program if test(new_program)
    end
    nil
  end

  def fix_program_at(cursor)
    @program.clone.tap do |new_program|
      new_program[cursor] = case new_program[cursor]
                            when /nop/
                              'jmp'
                            when /jmp/
                              'nop'
                            else
                              raise "unknown instruction"
                            end
    end
  end

  def test(program)
    computer = Computer.new(program)
    computer.run_until_visited
    computer.program_terminated?
  end
end
