module Airplane
  class Seat
    attr_reader :boarding_pass, :row, :column

    def initialize(boarding_pass)
      @boarding_pass = boarding_pass
      @row = parse_row
      @column = parse_column
    end

    def seat_id
      row * 8 + column
    end

    def self.all_seat_ids
      highest_seat_id = 127 * 8 + 7
      (0..highest_seat_id).to_a
    end

    private

    def parse_row
      boarding_pass[0..6].each_char.inject(0..127) do |row_range, region|
        case region
        when 'F'
          row_range.begin..(row_range.begin + row_range.size / 2 - 1)
        when 'B'
          (row_range.begin + row_range.size / 2)..row_range.end
        else
          raise "unknown region #{region}"
        end
      end.begin
    end

    def parse_column
      boarding_pass[7..9].each_char.inject(0..7) do |column_range, region|
        case region
        when 'L'
          column_range.begin..(column_range.begin + column_range.size / 2 - 1)
        when 'R'
          (column_range.begin + column_range.size / 2)..column_range.end
        else
          raise "unknown region #{region}"
        end
      end.begin
    end
  end
end
