#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/xmas'

data = File.read('day9_input').split("\n").map(&:to_i)
checker = Xmas::Checker.new(25)

# Part 1
first_invalid = checker.check_until_invalid(data)
puts "Part 1: #{first_invalid}"

# Part 2
contiguous_set = checker.find_contiguous_set_of_sum(data, first_invalid)
answer = contiguous_set.min + contiguous_set.max
puts "Part 2: #{answer}"
