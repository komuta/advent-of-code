#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/seating_system'

seats_map = File.read('day11_input')
seating_system = SeatingSystem.new(seats_map)

# Part 1
seating_system.stabilize
occupied_seats = seating_system.occupied_seats
puts "Part 1: #{occupied_seats}"
