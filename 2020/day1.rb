#!env ruby
# frozen_string_literal: true

require 'pry'

class TwoThousandTwentyFinder
  # @param entries [Array<Integer>]
  def initialize(entries)
    @entries = entries
  end

  def find(count)
    @entries.combination(count)
            .find { |tuple| tuple.sum == 2020 }
            .inject(1, &:*)
  end

  def self.read(file)
    TwoThousandTwentyFinder.new(
      File.read(file).split("\n").map(&:to_i)
    )
  end
end

puts TwoThousandTwentyFinder.read('day1_input').find(2)
puts TwoThousandTwentyFinder.read('day1_input').find(3)
