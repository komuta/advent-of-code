#!env ruby
# frozen_string_literal: true

require 'pry'
require_relative 'lib/bag_rules'

bags = File.read('day7_input').split("\n")
           .map(&BagRule.method(:parse))

# Part 1
count = BagFinder.new('shiny gold').find_in(bags).count
puts "Part 1: #{count}"

# Part 2
count = BagTree.new('shiny gold', bags).count - 1
puts "Part 2: #{count}"
